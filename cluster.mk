#
# Handles the OpenShift cluster
#

#
# Helper targets to login to OpenShift
#
login-test:
	oc login $(TESTINGCLUSTER) --password='' --username=''

login-prod:
	oc login $(PRODCLUSTER) --password='' --username=''

#
# Create the kubernetes project if it doesn't exist
#

check-project:
	@oc new-project $(PROJECT) || oc project $(PROJECT)
	@if [ "$$(oc project -q)" != "$(PROJECT)" ]; then
		echo "**** error: failed to switch to $(PROJECT)"
		exit 1
	fi

#
# Restart deployments
#

test_envs = $(shell CLUSTER='<Test|Prod>.Test' dhall-to-json --explain --file Dhall/Envs.dhall | jq 'map(select(.dbConfigName == "datomic-test-config")) | .[].name')
test_frontend_envs = $(shell CLUSTER='<Test|Prod>.Test' dhall-to-json --explain --file Dhall/Envs.dhall | jq 'map(select(.dbConfigName == "datomic-test-frontend-config")) | .[].name')
prod_read_envs = $(shell CLUSTER='<Test|Prod>.Prod' dhall-to-json --explain --file Dhall/Envs.dhall | jq 'map(select(.dbConfigName == "datomic-prod-config")) | .[].name')
env_error = "**** error: env variable ENV not set"

dbg-log-envs:
	@echo "test: $(test_envs)"
	@echo "frontend: $(test_frontend_envs)"
	@echo "prod: $(prod_read_envs)"

restart-test:
	$(MAKE) restart-envs ENVS="$(test_envs)"

restart-test-frontend:
	$(MAKE) restart-envs ENVS="$(test_frontend_envs)"

restart-prod-read:
	$(MAKE) restart-envs ENVS="$(prod_read_envs)"

restart-envs:
	@for env in $(ENVS); do
		$(MAKE) restart ENV=$$env
	done

restart: restart-api restart-cache

restart-api:
	@if [ -z $(ENV) ]; then echo $(env_error) && false ; else oc rollout restart deployment api-$(ENV) ; fi

restart-cache:
	@if [ -z $(ENV) ]; then echo $(env_error) && false ; else oc rollout restart deployment varnish-$(ENV) ; fi


