#
# Bootstrap the Taxonomy environment in OpenShift
#
# These commands needs to be run at least once when setting up the environment, and may
# require higher privileges.
#

#
# Bootstrap the environments in the Test and Prod clusters
#

bootstrap-env: | check-clean-repo check-project volume-world-writable

boot-volumes:
	# Datahike file backend
	oc apply -f database/claim.yaml
	oc apply -f database/create-volume.yaml

volume-world-writable: boot-volumes
	oc apply -f database/busybox-helper.yaml
	echo "sleep 30 seconds to wait for pod to start"
	sleep 30 # Wait for the pod to start
	oc get pod busybox-helper
	oc exec busybox-helper -- chmod 777 /mnt/datahike
	oc delete pod busybox-helper

#
# Bootstrap the build pipeline
#

bootstrap-build-pipeline: | check-clean-repo check-project
	@CLUSTERURL=$(shell $(CMD_CLUSTERURL)) && \
	if [ "$$CLUSTERURL" != "$(BUILDCLUSTER)" ]; then
		echo "**** error: not logged in to $(BUILDCLUSTER), but logged in to $$CLUSTERURL"
		exit 1
	else
		echo "Operating on $$CLUSTERURL."
	fi
	oc apply -f tekton/build-pipeline.yaml
	oc apply -f tekton/mattermost.yaml
	oc apply -f tekton/pipeline-cleaner.yaml
	oc apply -f tekton/pipeline-service-account.yaml
	kustomize build jobtech-taxonomy-api-deploy-secrets/build/ | oc apply -f -
	kustomize build jobtech-taxonomy-api-deploy-secrets/nexus/ | oc apply -f -
