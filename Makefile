#
# The main Makefile that includes all the specific ones.
#
# How to deploy an environment to test:
# make deploy-test - run Dhall compilation and oc apply on all test environments.
#

.ONESHELL:

# The project name in OpenShift
PROJECT = taxonomy-api-gitops

# Cluster URL's
TESTINGCLUSTER = "https://api.testing.services.jtech.se:6443"
PRODCLUSTER = "https://api.prod.services.jtech.se:6443"
BUILDCLUSTER = "https://api.build.services.jtech.se:6443"

# Determine which cluster we are logged in to
CMD_CLUSTERURL = TERM=dumb LANG=C oc whoami --show-server

# Include specific Makefiles
include repo.mk
include bootstrap.mk
include cluster.mk
include database.mk
include deploy.mk
