#
# Handles database migrations and upgrades
#

# Variables
prodwrite_db_file = build/prod_write.nippy

# Download the prod-write database
download-prodwrite-db:
	clj -X:task copy :src :datomic-prod-editor-config :dst '"$(prodwrite_db_file)"'

# Publish a new test database.
publish-test-db:
	clj -X:task copy :src '"$(prodwrite_db_file)"' :dst :datomic-test-config
	$(MAKE) restart-test

# Copy to a backup repository. Pass the repository path using an environment variable.
# BACKUP_REPO_ROOT=/home/ubuntu/prog/jobtech-taxonomy-backup-files make copy-to-backup-repo
copy-to-backup-repo:
	clj -X:task copy :src '"$(prodwrite_db_file)"' :dst '"$(BACKUP_REPO_ROOT)/prod-write/$(shell date +'%Y-%m-%d')/prod_write.nippy"'

# Publish a prodwrite copy database to frontend.
publish-frontend-test-db:
	clj -X:task copy :src '"$(prodwrite_db_file)"' :dst :datomic-test-frontend-config
	$(MAKE) restart-test-frontend

publish-prod-read-db:
	clj -X:task copy :src '"$(prodwrite_db_file)"' :dst :datomic-prod-config
	$(MAKE) restart-prod-read

publish-dev-local-db:
	clj -X:task copy :src '"$(prodwrite_db_file)"' :dst :datomic-dev-local-config :dbname-decorator '"$(DBNAME_DECORATOR)"'

output-configs:
	clj -X:task output-configs

clean-db-files:
	rm -rf $(prodwrite_db_file)

#
# Copy a Datahike database to the AWS EC2 filesystem
#

publish-nippy-database: check-project
	@if [ -z "$(FILENAME)" ]; then
		echo "**** error: env variable FILENAME not set"
		echo "usage: make publish-nippy-database FILENAME=database_v18.nippy"
		exit 1
	else
		oc apply -f database/busybox-helper.yaml
		echo "sleep 30 seconds to wait for pod to start"
		sleep 30 # Wait for the pod to start
		echo "copy database from $(FILENAME) to /mnt/datahike/"
		oc cp $(FILENAME) busybox-helper:/mnt/datahike
		echo "change permissions in folder /mnt/datahike/$$NAME"
		oc exec pod/busybox-helper -- chmod 666 /mnt/datahike/$(FILENAME)
		oc delete pod busybox-helper
	fi

copy-file-database: check-project
	@if [ -z "$(FOLDER)" ]; then
		echo "**** error: env variable FOLDER not set"
		echo "usage: make copy-file-database FOLDER=datahike-file-db"
		exit 1
	else
		oc apply -f database/busybox-helper.yaml
		echo "sleep 30 seconds to wait for pod to start"
		sleep 30 # Wait for the pod to start
		NAME=$(shell echo $(FOLDER) | sed 's/.*\///g')
		echo "copy database from $(FOLDER) to /mnt/datahike/$$NAME"
		oc cp $(FOLDER) busybox-helper:/mnt/datahike
		echo "change permissions in folder /mnt/datahike/$$NAME"
		oc exec pod/busybox-helper -- chmod -R 666 /mnt/datahike/$$NAME
		oc exec pod/busybox-helper -- chmod 777 /mnt/datahike/$$NAME
		oc delete pod busybox-helper
	fi
