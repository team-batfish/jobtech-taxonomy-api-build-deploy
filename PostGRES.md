# PostGRES on OpenShift

## Installation
- Go to the OperatorHub in OpenShift and Install: PostgreSQL Operator by Dev4Ddevs.com
- Go to Installed Operators and click Create Instance of "Database Database" API
- The instance should start a pod, note down the pod name

## Port Forwarding
```
oc port-forward <pod name> 27500:27500
```

## Create Database and User
```
$ psql -h localhost -p 27500 -U postgres
postgres=# create database datahike_backend;
postgres=# create user datahike;
postgres=# ALTER USER datahike WITH PASSWORD 'pwdatahike'; # Use the one from secrets (DATAHIKE_CFG__STORE__PASSWORD)
postgres=# grant all on database datahike_backend to datahike;
```

## PostGRES OpenShift
The following env variablbes need to be set in the Deployment of the database pod:
- POSTGRESQL_DATABASE
- POSTGRESQL_USER
- POSTGRESQL_PASSWORD

## Open a PostGRES Terminal
```
psql -h localhost -p 27500 -d datahike_backend -U datahike
```

## Start PostGRES in Docker
docker run --name some-postgres -e POSTGRES_PASSWORD=mysecretpassword -p 5432:5432 -d postgres

## Backup Strategy
Backups are handled with PostGRES tools, instead of Datahike.

```
pg_dump -h localhost -p 5432 -d datahike_backend -U datahike > dathike.sql
psql -h localhost -p 27500 -U datahike -d datahike_backend < dathike.sql
```

## Migration
The migration from Datomic to Datahike/PostGRES/OpenShift has to be done in two steps at the moment.
- Migrate from Datomic to a local PostGRES running in Docker, using wanderung.
- Use the commands from the Backup strategy to move the database from the Docker container to OpenShift.
