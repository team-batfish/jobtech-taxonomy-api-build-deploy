#
# Handles the gitops repo
#

# Usage: make set-api-image SHA=35fe8ad53bad8358f774cf557e1ce4c799238315
# Sets the deployment image for i1, u1 and frontend
DHALL_CONFIG = Dhall/Envs.dhall
set-api-image:
	@if [ -z "$(SHA)" ]; then
		echo "**** error: env variable SHA not set"
		exit 1
	else
		sed -i.old "s/apiImageTest.*= \".*\"/apiImageTest : Text     = \"$(SHA)\"/g" $(DHALL_CONFIG)
	fi

promote-from-test:
	@API_SHA=$(shell grep "let.*apiImageTest" $(DHALL_CONFIG) | grep -o "\".*\"" | tr -d '"') && \
	VARNISH_SHA=$(shell grep "let.*varnishImageTest" $(DHALL_CONFIG) | grep -o "\".*\"" | tr -d '"') && \
	sed -i.old "s/apiImageProd.*= \".*\"/apiImageProd : Text     = \"$$API_SHA\"/g" $(DHALL_CONFIG) && \
	sed -i.old "s/varnishImageProd.*= \".*\"/varnishImageProd : Text = \"$$VARNISH_SHA\"/g" $(DHALL_CONFIG)

check-clean-repo: gitcheck check-secrets

gitcheck:
	@if [ ! -z "$$(git status --porcelain)" ]; then
		echo "**** error: repo is not clean"
		exit 1
	fi
	@if [ "$$(LANG=C git rev-parse --symbolic-full-name --abbrev-ref HEAD)" != "master" ]; then
		echo "**** error: not on the master branch"
		exit 1
	fi
	@git push

check-secrets:
	@git submodule update --init --recursive
	@if [ ! -d "jobtech-taxonomy-api-deploy-secrets" ]; then
		echo "**** error: secrets are missing"
		exit 1
	fi
