{-
  Generates routes for the API and Varnish servers
-}

--| Import the Prelude
let Prelude = ./Prelude.dhall

let not = Prelude.Bool.not

let map = Prelude.List.map

let filter = Prelude.List.filter

let default = Prelude.Optional.default

let OpenShift = ./OpenShift.dhall

let cfg = ./Config.dhall

let envs = ./Envs.dhall

let utils = ./Utils.dhall

let route
    : Text -> Text -> Natural -> Text -> OpenShift.Route.Type
    = \(app : Text) ->
      \(name : Text) ->
      \(port : Natural) ->
      \(host : Text) ->
        let port-str
            : Text
            = "${Natural/show port}-tcp"

        in  OpenShift.Route::{
            , metadata = OpenShift.ObjectMeta::{
              , name = Some name
              , labels = Some (toMap { app })
              , annotations = Some
                [ { mapKey = "haproxy.router.openshift.io/timeout"
                  , mapValue = "120s"
                  }
                ]
              }
            , spec = OpenShift.RouteSpec::{
              , host
              , port = Some OpenShift.RoutePort::{
                , targetPort = OpenShift.IntOrString.String port-str
                }
              , to = OpenShift.RouteTargetReference::{
                , kind = "Service"
                , name
                , weight = 100
                }
              }
            }

let addIngress
    : Text -> OpenShift.Route.Type -> OpenShift.Route.Type
    = \(name : Text) ->
      \(r : OpenShift.Route.Type) ->
        let lbls = r.metadata.labels

        let MapList = List { mapKey : Text, mapValue : Text }

        let newValue =
              default MapList ([] : MapList) lbls # toMap { type = name }

        in  r
          with metadata.labels = Some newValue

let addTls
    : OpenShift.TLSConfig.Type -> OpenShift.Route.Type -> OpenShift.Route.Type
    = \(tls : OpenShift.TLSConfig.Type) ->
      \(r : OpenShift.Route.Type) ->
        r
        with spec.tls = Some tls

let routeApi
    : cfg.EnvConfig -> OpenShift.Route.Type
    = \(c : cfg.EnvConfig) ->
        let r =
              route (utils.appName c.name) (utils.apiName c.name) cfg.apiPort ""

        in  if    c.apiHasTlsTermination
            then  let tls =
                        OpenShift.TLSConfig::{
                        , termination = "edge"
                        , insecureEdgeTerminationPolicy = Some "Allow"
                        }

                  in  addTls tls (addIngress "api-jobtechdev-se-route" r)
            else  r

let routeVarnish
    : cfg.EnvConfig -> OpenShift.Route.Type
    = \(c : cfg.EnvConfig) ->
        let tls =
              OpenShift.TLSConfig::{
              , termination = "edge"
              , insecureEdgeTerminationPolicy = Some "Allow"
              }

        let r =
              route
                (utils.appName c.name)
                (utils.varnishName c.name)
                cfg.varnishPort
                (utils.hostName c.name)

        in  addTls tls (addIngress "api-jobtechdev-se-route" r)

let removeTlsTerminationRoutes =
      \(c : cfg.EnvConfig) -> not c.apiHasTlsTermination

in    map cfg.EnvConfig OpenShift.Route.Type routeApi envs
    # map
        cfg.EnvConfig
        OpenShift.Route.Type
        routeVarnish
        (filter cfg.EnvConfig removeTlsTerminationRoutes envs)
