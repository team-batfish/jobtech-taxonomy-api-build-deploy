{-
  The configuration file for the Taxonomy API environments
-}

--| Import the Config
let Config = ./Config.dhall
let EnvConfig = Config.EnvConfig
let cluster = Config.cluster

--| Do not format these 4 lines of SHA values, the Makefile depends on the formatting for now
let apiImageTest : Text     = "f3d9bf7d"
let apiImageProd : Text     = "9a24218756a491171d1d9c956e19e6ed72d56b0a"
let varnishImageTest : Text = "d95cf8ec"
let varnishImageProd : Text = "e1cee22fbe068db1aafe9d9d408dcc0829fc4291"

let defaultTestConfig
    : Text -> EnvConfig
    = \(name : Text) ->
        { name
        , imageApi = apiImageTest
        , nbrApiPods = 1
        , imageVarnish = varnishImageTest
        , nbrVarnishPods = 1
        , dbConfigName = "nippy-in-mem-config"
        , volumeMount = True
        , useResourceLimits = False
        , apiHasTlsTermination = False
        }

let defaultProdConfig
    : Text -> EnvConfig
    = \(name : Text) ->
            defaultTestConfig name
        //  { imageApi = apiImageProd
            , imageVarnish = varnishImageProd
            , dbConfigName = "datomic-prod-config"
            }

let config
    : List EnvConfig
    = merge
        { Test =
          [     defaultTestConfig "frontend"
            //  { dbConfigName = "datomic-test-frontend-config"
                , apiHasTlsTermination = True
                }
          , defaultTestConfig "u1"
          , defaultTestConfig "i1"
          , defaultTestConfig "t1"
          , defaultTestConfig "t2" // { nbrApiPods = 5, nbrVarnishPods = 3 }
          ]
        , Prod =
          [ defaultProdConfig "prod" // { nbrApiPods = 8, nbrVarnishPods = 3 }
          ,     defaultProdConfig "editor-prod"
            //  { nbrApiPods = 5
                , dbConfigName = "datomic-prod-editor-config"
                , apiHasTlsTermination = True
                }
          ,     defaultProdConfig "dh"
            //  { dbConfigName = "prod-in-mem-config"
                , useResourceLimits = False
                }
          ]
        }
        cluster

in  config
