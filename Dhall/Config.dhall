{-
  The configuration file for the Taxonomy API environments
-}

--| Import the Prelude
let Prelude = ./Prelude.dhall

--| Set environment variable CLUSTER to select environment config
--  See Makefile for details.
let Cluster = < Test | Prod >

let optCluster
    : Optional Cluster
    = Some env:CLUSTER ? None Cluster

let cluster
    : Cluster
    = Prelude.Optional.default Cluster Cluster.Test optCluster

let nexusUrl
    : Text
    = "docker-images.jobtechdev.se"

--| The base URL to images for the API server
let apiImageUrl
    : Text
    = "${nexusUrl}/jobtech-taxonomy-api/jobtech-taxonomy-api"

--| The base URL to images for the Varnish server
let varnishImageUrl
    : Text
    = "${nexusUrl}/openshift-varnish/openshift-varnish"

--| The base name for the apps
let appBaseName
    : Text
    = "taxonomy-app"

--| The base name for the API
let baseName
    : Text
    = "api"

--| The port used by the API server
let apiPort
    : Natural
    = 3000

--| The port used by the Varnish server
let varnishPort
    : Natural
    = 8083

--| A `EnvConfig` type used to hold a configuration for an environment, e.g. the U1 environment
let EnvConfig
    : Type
    = { name : Text
      , imageApi : Text
      , nbrApiPods : Natural
      , imageVarnish : Text
      , nbrVarnishPods : Natural
      , dbConfigName : Text
      , volumeMount : Bool
      , useResourceLimits : Bool
      , apiHasTlsTermination : Bool
      }

--| Export necessary configuration types and constants
in  { EnvConfig
    , apiPort
    , varnishPort
    , appBaseName
    , baseName
    , apiImageUrl
    , varnishImageUrl
    , cluster
    }
