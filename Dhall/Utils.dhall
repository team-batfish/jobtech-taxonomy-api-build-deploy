{-
  A collection of utilities
-}

--| Import the Prelude
let Prelude = ./Prelude.dhall

let replace = Prelude.Text.replace

let cfg = ./Config.dhall

--| Generate the app name based on the environment name (`env`)
let appName
    : Text -> Text
    = \(env : Text) -> "${cfg.appBaseName}-${env}"

--| Generate the API name based on the environment name (`env`)
let apiName
    : Text -> Text
    = \(env : Text) -> "${cfg.baseName}-${env}"

--| Generate the API URL based on the environment name (`env`)
let apiUrl
    : Text -> Text
    = \(env : Text) ->
        "${cfg.baseName}-${env}.taxonomy-api-gitops.svc.cluster.local"

--| Generate the Varnish name based on the environment name (`env`)
let varnishName
    : Text -> Text
    = \(env : Text) -> "varnish-${env}"

--| Generate the route host name based on the environment name (`env`)
--  Removes '-prod' from the URL so the PROD environment gets URL taxonomy.api.jobtechdev.se
let hostName
    : Text -> Text
    = \(env : Text) ->
        replace "-prod.api" ".api" "taxonomy-${env}.api.jobtechdev.se"

--| Generate the labels
let labels
    : Text -> Text -> Optional (List { mapKey : Text, mapValue : Text })
    = \(app : Text) -> \(service : Text) -> Some (toMap { app, service })

--| Export the utility functions
in  { appName, apiName, apiUrl, varnishName, hostName, labels }
