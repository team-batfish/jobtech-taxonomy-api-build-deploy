{-
  Import the OpenShift bindings only in one place
-}

--| Import OpenShift bindings
let OpenShift =
      https://raw.githubusercontent.com/TristanCacqueray/dhall-openshift/4f908bb2c7c5b3c1bc4dbb8afb3c0744d416aa02/package.dhall
        sha256:a6883f73597b7dfbded7638222562c2fb0b96d30089fa8e32fa4ea43a487b32e

in  OpenShift
