{-
  Configure the database backends for the Taxonomy API Server
-}
let kube = ./Kubernetes.dhall

let cfg = ./Config.dhall

let Prelude = ./Prelude.dhall

let concat = Prelude.List.concat

let default = Prelude.Optional.default

let mergeConfigs
    : kube.ConfigMap.Type -> kube.ConfigMap.Type -> kube.ConfigMap.Type
    = \(cfgBase : kube.ConfigMap.Type) ->
      \(cfg : kube.ConfigMap.Type) ->
        let Map = { mapKey : Text, mapValue : Text }

        let baseData = default (List Map) ([] : List Map) cfgBase.data

        let mergedData = default (List Map) ([] : List Map) cfg.data

        in  cfgBase
          with data = Some (concat Map [ baseData, mergedData ])

let createDatomicConfig
    : Text -> Text -> Text -> Text -> Text -> kube.ConfigMap.Type
    = \(configName : Text) ->
      \(region : Text) ->
      \(system : Text) ->
      \(endpoint : Text) ->
      \(dbName : Text) ->
        kube.ConfigMap::{
        , metadata = kube.ObjectMeta::{ name = Some configName }
        , data = Some
            ( toMap
                { DATOMIC_CFG__REGION = region
                , DATOMIC_CFG__SYSTEM = system
                , DATOMIC_CFG__ENDPOINT = endpoint
                , DATOMIC_CFG__DATOMIC_NAME = dbName
                }
            )
        }

let databaseVersionTest
    : Text
    = "v19"

let datomicRegion
    : Text
    = "eu-central-1"

let datomicTestSystem
    : Text
    = "tax-test-v4"

let datomicProdSystem
    : Text
    = "tax-prod-v4"

let datomicTestEndpoint
    : Text
    = "https://hcvc9p1nvi.execute-api.eu-central-1.amazonaws.com"

let datomicProdEndpoint
    : Text
    = "http://tax-c-loadb-2ujixczkeo0e-4baa81991e5ad6eb.elb.eu-central-1.amazonaws.com:8182"

let datomicTestConfig
    : kube.ConfigMap.Type
    = createDatomicConfig
        "datomic-test-config"
        datomicRegion
        datomicTestSystem
        datomicTestEndpoint
        "jobtech-taxonomy-dev-2023-03-02-16-40-48"

let datomicTestFrontendConfig
    : kube.ConfigMap.Type
    = createDatomicConfig
        "datomic-test-frontend-config"
        datomicRegion
        datomicTestSystem
        datomicTestEndpoint
        "jobtech-taxonomy-frontend-2023-03-02-16-27-15"

let datomicProdConfig
    : kube.ConfigMap.Type
    = createDatomicConfig
        "datomic-prod-config"
        datomicRegion
        datomicProdSystem
        datomicProdEndpoint
        "jobtech-taxonomy-prod-2023-03-02-17-17-19"

let datomicProdEditorConfig
    : kube.ConfigMap.Type
    = createDatomicConfig
        "datomic-prod-editor-config"
        datomicRegion
        datomicProdSystem
        datomicProdEndpoint
        "jobtech-taxonomy-prod-write-2023-04-03-14-12-01"

let datomicDevLocalConfig
    : kube.ConfigMap.Type
    = createDatomicConfig
        "datomic-dev-local-config"
        datomicRegion
        datomicTestSystem
        datomicTestEndpoint
        "jobtech-taxonomy-david-test-2022-11-08-14-31-37"

let createDatahikeFileConfig
    : Text -> Text -> kube.ConfigMap.Type
    = \(configName : Text) ->
      \(path : Text) ->
        kube.ConfigMap::{
        , metadata = kube.ObjectMeta::{ name = Some configName }
        , data = Some
            ( toMap
                { DATAHIKE_CFG__STORE__BACKEND = ":file"
                , DATAHIKE_CFG__STORE__PATH = path
                , DATABASE_BACKEND = "datahike"
                }
            )
        }

let createNippyConfig
    : Text -> Text -> kube.ConfigMap.Type
    = \(configName : Text) ->
      \(filename : Text) ->
        kube.ConfigMap::{
        , metadata = kube.ObjectMeta::{ name = Some configName }
        , data = Some
            ( toMap
                { NIPPY_CFG__FILENAME = filename, DATABASE_BACKEND = "nippy" }
            )
        }

let setConfigName
    : Text -> kube.ConfigMap.Type -> kube.ConfigMap.Type
    = \(name : Text) ->
      \(config : kube.ConfigMap.Type) ->
        config
        with metadata.name = Some name

let inMem
    : kube.ConfigMap.Type -> kube.ConfigMap.Type
    = \(config : kube.ConfigMap.Type) ->
        let Map = { mapKey : Text, mapValue : Text }

        let oldData = default (List Map) ([] : List Map) config.data

        let inMemMap = toMap { IN_MEM = "true" }

        in  config
          with data = Some (concat Map [ oldData, inMemMap ])

let datahikeTestFileConfig
    : kube.ConfigMap.Type
    = createDatahikeFileConfig
        "datahike-test-file-config"
        "/mnt/datahike/taxonomy_db_version_13"

let inMemConfig
    : kube.ConfigMap.Type
    = setConfigName "in-mem-config" (inMem datomicTestConfig)

let nippyInMemConfigBase
    : kube.ConfigMap.Type
    = createNippyConfig
        "nippy-in-mem-config"
        "/mnt/datahike/database_${databaseVersionTest}.nippy"

let nippyInMemConfig
    : kube.ConfigMap.Type
    = mergeConfigs nippyInMemConfigBase datomicTestConfig

let prodInMemConfig
    : kube.ConfigMap.Type
    = setConfigName "prod-in-mem-config" (inMem datomicProdConfig)

in  merge
      { Test =
        [ datomicTestConfig
        , datomicTestFrontendConfig
        , datomicDevLocalConfig
        , datahikeTestFileConfig
        , inMemConfig
        , nippyInMemConfigBase
        ]
      , Prod = [ datomicProdConfig, datomicProdEditorConfig, prodInMemConfig ]
      }
      cfg.cluster
