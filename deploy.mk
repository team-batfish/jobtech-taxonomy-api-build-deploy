#
# Handle building the configuration and deploying it to OpenShift
#

#
# Build the configuration files
#
CONFIG_FILES = \
    ServiceAccounts.dhall \
    Backends.dhall \
    Deployments.dhall \
    Routes.dhall \
    Java.dhall \
    Services.dhall

UTILS = \
    Envs.dhall \
    Config.dhall \
    Kubernetes.dhall \
    Prelude.dhall \
    OpenShift.dhall \
    Utils.dhall

test-%.yaml: $(addprefix Dhall/,${CONFIG_FILES} ${UTILS})
	CLUSTER='<Test|Prod>.Test' dhall-to-yaml --documents --explain --file $(subst test-,Dhall/,$(patsubst %.yaml,%.dhall,$@)) --output $@

prod-%.yaml: $(addprefix Dhall/,${CONFIG_FILES} ${UTILS})
	CLUSTER='<Test|Prod>.Prod' dhall-to-yaml --documents --explain --file $(subst prod-,Dhall/,$(patsubst %.yaml,%.dhall,$@)) --output $@

clean:
	rm -f test-*.yaml prod-*.yaml

build-test-config: $(addprefix test-,$(CONFIG_FILES:.dhall=.yaml))

build-prod-config: $(addprefix prod-,$(CONFIG_FILES:.dhall=.yaml))

#
# Deploy the environment configuration files
#

deploy-test: | check-project setup-test build-test-config deploy-gitops-sha
	@for yaml in $(addprefix test-,$(patsubst %.dhall,%.yaml,${CONFIG_FILES})); do
		echo "applying $$yaml"
		oc apply -f $$yaml
	done

deploy-prod: | check-clean-repo check-project setup-prod build-prod-config deploy-gitops-sha
	@for yaml in $(addprefix prod-,$(patsubst %.dhall,%.yaml,${CONFIG_FILES})); do
		echo "applying $$yaml"
		oc apply -f $$yaml
	done

#
# Deploy the gitops version used to deploy to OpenShift
#

deploy-gitops-sha:
	@SHA=$(shell git rev-parse HEAD) && \
	DTYVERSION=$(shell dhall-to-yaml --version) && \
	DVERSION=$(shell dhall --version) && \
	sed "s/SHAVALUE/$$SHA/" gitops-sha.yaml | \
	sed "s/DTYVERSION/$$DTYVERSION/" | \
	sed "s/DVERSION/$$DVERSION/" | oc apply -f -

#
# Deploy secrets and Nexus configurations
#

setup-test:
	@CLUSTERURL=$(shell $(CMD_CLUSTERURL)) && \
	if [ "$$CLUSTERURL" != "$(TESTINGCLUSTER)" ]; then
		echo "**** error: not logged in to $(TESTINGCLUSTER), but logged in to $$CLUSTERURL"
		exit 1
	else
		echo "Operating on $$CLUSTERURL."
	fi
	kustomize build jobtech-taxonomy-api-deploy-secrets/test/ | oc apply -f -
	kustomize build jobtech-taxonomy-api-deploy-secrets/nexus/ | oc apply -f -

setup-prod:
	@CLUSTERURL=$(shell $(CMD_CLUSTERURL)) && \
	if [ "$$CLUSTERURL" != "$(PRODCLUSTER)" ]; then
		echo "**** error: not logged in to $(PRODCLUSTER), but logged in to $$CLUSTERURL"
		exit 1
	else
		echo "Operating on $$CLUSTERURL."
	fi
	kustomize build jobtech-taxonomy-api-deploy-secrets/prod/ | oc apply -f -
	kustomize build jobtech-taxonomy-api-deploy-secrets/nexus/ | oc apply -f -
