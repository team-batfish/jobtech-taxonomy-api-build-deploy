(ns jobtech-taxonomy-api-gitops.rewrite-history.add-ids-to-relations
  (:require [jobtech-taxonomy-common.relation :as relation]
            [jobtech-taxonomy-api-gitops.rewrite-history.datom :as datom]))

(defn transform [datoms]
  (let [concept-e->id (->> datoms
                           (filter #(-> % datom/a #{:concept/id}))
                           (map (juxt datom/e datom/v))
                           (into {}))
        tx-partitioned-datoms (->> datoms
                                   (group-by datom/tx)
                                   (sort-by key)
                                   (map val))
        first-tx (datom/tx (ffirst tx-partitioned-datoms))]
    (->> tx-partitioned-datoms
         (mapcat
           (fn [datoms-by-tx]
             (let [tx (datom/tx (first datoms-by-tx))]
               ;; this code adds relation/id datoms to corresponding txs
               (into datoms-by-tx
                     (->> datoms-by-tx
                          (filter #(-> % datom/a #{:relation/type
                                                   :relation/concept-1
                                                   :relation/concept-2}))
                          (group-by datom/e)
                          (vals)
                          (map (fn [rel-datoms]
                                 (let [e (datom/e (first rel-datoms))
                                       added (datom/added (first rel-datoms))
                                       attrs (->> rel-datoms
                                                  (map (juxt datom/a datom/v))
                                                  (into {}))]
                                   [e
                                    :relation/id
                                    (relation/id
                                      (concept-e->id (:relation/concept-1 attrs))
                                      (:relation/type attrs)
                                      (concept-e->id (:relation/concept-2 attrs)))
                                    tx
                                    added]))))))))
         (into [[":relation/id" :db/ident :relation/id first-tx true]
                [":relation/id" :db/cardinality :db.cardinality/one first-tx true]
                [":relation/id" :db/valueType :db.type/string first-tx true]
                [":relation/id" :db/unique :db.unique/identity first-tx true]
                [":relation/id" :db/doc "Unique identifier for relations" first-tx true]
                [0 :db.install/attribute ":relation/id" first-tx true]]))))