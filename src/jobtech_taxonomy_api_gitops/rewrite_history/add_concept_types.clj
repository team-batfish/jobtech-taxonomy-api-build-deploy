(ns jobtech-taxonomy-api-gitops.rewrite-history.add-concept-types
  (:require [jobtech-taxonomy-api-gitops.rewrite-history.datom :as datom]))

(defn transform [datoms]
  (let [first-tx (datom/tx (first datoms))]
    (concat
      [[":concept-type/id" :db/ident :concept-type/id first-tx true]
       [":concept-type/id" :db/cardinality :db.cardinality/one first-tx true]
       [":concept-type/id" :db/valueType :db.type/string first-tx true]
       [":concept-type/id" :db/unique :db.unique/identity first-tx true]
       [":concept-type/id" :db/doc "Unique identifier for concept types" first-tx true]
       [0 :db.install/attribute ":concept-type/id" first-tx true]

       [":concept-type/label-sv" :db/ident :concept-type/label-sv first-tx true]
       [":concept-type/label-sv" :db/cardinality :db.cardinality/one first-tx true]
       [":concept-type/label-sv" :db/valueType :db.type/string first-tx true]
       [":concept-type/label-sv" :db/doc "Swedish label for concept types" first-tx true]
       [0 :db.install/attribute ":concept-type/label-sv" first-tx true]

       [":concept-type/label-en" :db/ident :concept-type/label-en first-tx true]
       [":concept-type/label-en" :db/cardinality :db.cardinality/one first-tx true]
       [":concept-type/label-en" :db/valueType :db.type/string first-tx true]
       [":concept-type/label-en" :db/doc "English label for concept types" first-tx true]
       [0 :db.install/attribute ":concept-type/label-en" first-tx true]]
      datoms)))